export class Sitios {
    idSite: number;
    nameSite: string;
    domain: string;
    email : string;
    absolutePath : string
    constructor (sitios?: {
        idSite: number;
        nameSite: string;
        domain: string;
        email : string;
        absolutePath : string;
    }) {
        if (sitios) {
            
            this.idSite  =  sitios.idSite
            this.nameSite =  sitios.nameSite
            this.domain = sitios.domain
            this. email = sitios.email
            this.absolutePath = sitios.absolutePath
            
            
        } else {
            this.idSite  =  null
            this.nameSite = null
            this.domain = null
            this. email = null
            this.absolutePath =null
        }
    }

}
