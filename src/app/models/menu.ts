export class Menu {
    idKey: number;
    idSite: number;
    name: string;
    listOrder: number;
    father: number;
    abm: string;
    link: string;

    constructor (Menu?: {
        idKey: number;
        idSite: number;
        name: string;
        listOrder: number;
        father: number;
        abm: string;
        link: string
    }) {
        if (Menu) {
            this.idKey = Menu.idKey;
            this.idSite = Menu.idSite;
            this.name = Menu.name;
            this.listOrder = Menu.listOrder;
            this.father = Menu.father;
            this.abm = Menu.abm;
            this.link = Menu.link;
        } else {
             this.idKey =null;
            this.idSite = null;
            this.name = null;
            this.listOrder = null;
            this.father = null;
            this.abm = null;
            this.link = null;
        }
    }

}