export class Parametro {
    idParameter: number;
    idCompany: number;
    name: string;
    description: string;
    value: string;
    function: number;
    status: number;
    
    constructor(parametro?: {
        idParameter: number;
        idCompany: number;
        name: string;
        description: string;
        value: string;
        function: number;
        status: number;
    }) {
        if (parametro) {
            this.idParameter = parametro.idParameter;
            this.idCompany = parametro.idCompany;
            this.name = parametro.name;
            this.description = parametro.description;
            this.value = parametro.value;
            this.function = parametro.function;
            this.status = parametro.status;
          
            
            
            
            
        } else {
            this.idParameter =null;
            this.idCompany = null;
            this.name = null;
            this.description =null;
            this.value =null;
            this.function = null;
            this.status = null;
        }
    }

}