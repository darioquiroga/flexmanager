export class Token {
    idKey: number;
    token: string;
    checkIn: string;
    checkOut : string;
    tokenStart : string;
    tokenEnd : string;
    tokenMessage : string;
    response : boolean;
    
    constructor (token?: {
        idKey: number;
        token: string;
        checkIn: string;
        checkOut : string;
        tokenStart : string;
        tokenEnd : string;
        tokenMessage : string;
        response : boolean;
    }) {
        if (token) {
            this.idKey = token.idKey;
            this.token = token.token;
            this.checkIn = token.checkIn;
            this.checkOut = token.checkOut;
            this.tokenStart  = token.tokenStart;
            this.tokenEnd  = token.tokenEnd;
            this.tokenMessage  = token.tokenMessage;
            this.response = token.response;
        } else {
            this.idKey = null;
            this.token = null;
            this.checkIn = null;
            this.checkOut = null;
            this.tokenStart  = null;
            this.tokenEnd  = null;
            this.tokenMessage  = null;
            this.response = null;
        }
    }

}
