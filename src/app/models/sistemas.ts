export class Sistemas {
    idSys: number;
    name: string;
    description: string;
    url : string;
    path : string;
    dbName : string;
    dbUser : string;
    dbPassword : string;
    dbConnectOption : string;
    dbPortNumber : string;
    typeName  :string
   
    
    
    
    
    
    constructor (sistemas?: {
        idSys: number;
        name: string;
        description: string;
        url : string;
        path : string;
        dbName : string;
        dbUser : string;
        dbPassword : string;
        dbConnectOption : string;
        dbPortNumber : string;
        typeName  :string;
    }) {
        if (sistemas) {
            this.idSys = this.idSys;
            this.name = this.name;
            this.description = this.description;
            this.url = this.url;
            this.path = this.path;
            this.dbName = this.dbName;
            this. dbUser = this.dbUser;
            this.dbPassword = this.dbPassword;
            this.dbConnectOption = this.dbConnectOption;
            this.dbPortNumber = this.dbPortNumber;
            this.typeName  = this.typeName;
         } else {
            this.idSys = null;
            this.name = null;
            this.description = null;
            this.url = null;
            this.path = null;
            this.dbName = null;
            this. dbUser = null;
            this.dbPassword = null;
            this.dbConnectOption = null;
            this.dbPortNumber = null;
            this.typeName  = null;
        }
    }

}
