import { Perfil } from "./perfil";
import {Sitios} from "./sitios";
import { Menu } from "./menu";
import { Sistemas } from "./sistemas";
import { Token } from "./token";



export class Operador {
    idKey: number;
    idCompany: number;
    name: string;
    username:string;
    password:string;
    middlename: string;
    lastname: string;
    email:string;
    Perfil: Perfil;
    Menu: Menu;
    Sitios: Sitios;
    Sistemas: Sistemas;
    Token: Token;
    constructor (operador?: {
        idKey: number,
        idCompany: number, 
        name:string, 
        middlename:string,
        username:string;
        password:string, 
        lastname: string,
        email:string,
        Token: {
            idKey: number,
            token: string,
            checkIn: string,
            checkOut : string,
            tokenStart : string,
            tokenEnd : string,
            tokenMessage : string,
            response : boolean;
        },
        Sitios: {
              idSite: number,
              nameSite: string,
              email : string,
              absolutePath : string,    
              domain: string,     
              Perfil: {
                    idKeyRela: number,
                    idGroup : number,
                    nameGroup: string,
                    descriptionGroup: string

              },
              Menu: {
                    idKey: number,
                    idSite : number,
                    name: string,
                    listOrder: number,
                    father: number,
                    abm: number,
                    llink : string

              },    
         },
        Sistemas: {
            idSys: number,
            name: string,
            description: string,
            url : string,
            path : string,
            dbName : string,
            dbUser : string,
            dbPassword : string,
            dbConnectOption : string,
            dbPortNumber : string,
            typeName  :string,
        }
    }
    ) {
        if (operador) {
            this.idKey = operador.idKey;
            this.email = operador.email;
            this.name = operador.lastname+" "+operador.middlename;
            this.username = operador.username;
            this.password = operador.password;
            this.idCompany = operador.idCompany
            //this.perfil = new Perfil(operador.perfil);
        } else {
            this.idKey = null;
            this.email =null;
            this.name =null;
            this.username = null;
            this.password = null;
            this.idCompany = null;
            //this.perfil = new Perfil();
        }
    }

}