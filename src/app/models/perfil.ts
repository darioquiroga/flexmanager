export class Perfil {
    idKey: number;
    name: string;
    descripcion: string;
    status: string;
    constructor (perfil?: {
        idKey: number;
        name: string;
        descripcion: string;
        status: string;
        
    }) {
        if (perfil) {
            this.idKey = perfil.idKey;
            this.name = perfil.name;
            this.descripcion = perfil.descripcion;
            this.status = perfil.status;
        } else {
            this.idKey = null;
            this.name = null;
            this.descripcion = null;
            this.status = null;
        }
    }
 }