import { Empresa } from 'app/models/empresa';
import { Operador } from 'app/models/operador';
import { Perfil } from 'app/models/perfil';
import { Parametro } from 'app/models/parametro';
import { Sistemas } from 'app/models/sistemas';
import { Sitios } from 'app/models/sitios';
import { Token } from 'app/models/token';

const classes = {
    Empresa, 
    Operador,
    Perfil,
    Parametro,
    Sistemas,
    Sitios,
    Token
};

export default function dynamicClass (name) {
    return classes[name];
}
