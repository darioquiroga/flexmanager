import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { AppTranslationModule } from '../../../app.translation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../../theme/nga.module';

import { Login } from './login.component';
import { routing }       from './login.routing';
import { LoginService } from '../../../services/loginservice';

import { AuthService } from '../../../services/authService';
import { UtilsService } from '../../../services/utilsService';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    HttpClientModule,
    routing
  ],
  declarations: [
    Login
  ],
  providers: [
    BrowserModule,
    HttpClientModule,
    LoginService,
    AuthService,
    UtilsService,
  ],
  
})
export class LoginModule {}
