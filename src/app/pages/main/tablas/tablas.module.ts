import { NgModule } from '@angular/core';
import { routing } from './tablas.routing';
import { Tablas } from 'app/pages/main/tablas';

import { DataTableModule } from "angular2-datatable";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../../theme/nga.module';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { DataTables } from '../../reusable/tablas/dataTables';
import { DataFilterPipe } from '../../reusable/tablas/dataTables/data-filter.pipe';
import { DataTablesService } from '../../reusable/tablas/dataTables/dataTables.service';
import { CustomCard } from '../../reusable/cards/customCard';

// operadores /////////
import { Operadores } from './operadores';
import { NuevoOperador } from './operadores/components/nuevoOperador';
import { EditarOperador } from './operadores/components/editarOperador';
// fin operadores ///
import { AuthService } from '../../../services/authService';
import { LocalStorageService } from '../../../services/localStorageService';
import { UtilsService } from '../../../services/utilsService';
import { RecursoService } from '../../../services/recursoService';
import { SharedModule } from '../SharedModule';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        routing,
        // DataTableModule,
        // CommonModule,
        // FormsModule,
        // NgaModule,
        // NgbDatepickerModule,
        SharedModule
    ],
    declarations: [
        Tablas,
        Operadores,
        NuevoOperador,
        EditarOperador
       
    ],
    providers: [
        HttpClientModule,
        DataTablesService,
        AuthService,
        LocalStorageService,
        UtilsService,
        RecursoService
    ]
})
export class TablasModule {
}
