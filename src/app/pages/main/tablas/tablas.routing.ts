import { Routes, RouterModule } from '@angular/router';

import { Tablas } from './tablas.component';
import { Operadores } from './operadores';
import { NuevoOperador } from 'app/pages/main/tablas/operadores/components/nuevoOperador';
import { EditarOperador } from 'app/pages/main/tablas/operadores/components/editarOperador';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
    {
        path: '',
        component: Tablas,
        children: [
            { path: 'operador', component: Operadores },
            { path: 'operador/nuevo', component: NuevoOperador },
            { path: 'operador/editar/:idOperador', component: EditarOperador },
           
        ]
    }
];

export const routing = RouterModule.forChild(routes);
