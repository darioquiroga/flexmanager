import { Component, Input } from '@angular/core';

import { LocalStorageService } from '../../../../../../services/localStorageService';
import { Operador } from '../../../../../../models/operador';
import { Sitios } from 'app/models/sitios';
import { Perfil } from '../../../../../../models/perfil';
import { Sistemas } from '../../../../../../models/sistemas';
import { environment } from 'environments/environment';
import { UtilsService } from '../../../../../../services/utilsService';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RecursoService } from '../../../../../../services/recursoService';
import { resourcesREST } from 'constantes/resoursesREST';

import * as crypto from 'crypto-js';

@Component({
    selector: 'editar-operador',
    styleUrls: ['./editarOperador.scss'],
    templateUrl: './editarOperador.html',
})
export class EditarOperador {
    // oPERADOR que se va a editar
    recurso: Operador = new Operador();
    // Perfiles disponible para tal sucursal
    perfiles: Observable<Perfil[]>;
    // Perfiles disponible para tal sitios
    sitios: Observable<Sitios[]>;
    // Perfiles disponible para tal sistemas
    sistemas: Observable<Sistemas[]>;
    constructor(
        private utilsService: UtilsService,
        private router: Router,
        private route: ActivatedRoute,
        private recursoService: RecursoService,
        private localStorageService: LocalStorageService
    ) {
        // Obtengo las sitios disponibles de la empresa
        // this.sitios = recursoService.getRecursoList(resourcesREST.sucursales)();
        
        // Busco el id del usuario a editar en la ruta
        /*this.route.params.subscribe(params => {
            
            // Obtengo el usuario que se va a editar
            this.recursoService.getRecursoList(resourcesREST.operador)()
                .map((recursoList: Usuario[]) =>
                    recursoList.find(operador => operador.idKey === parseInt(params.idKey))
                )
                .subscribe(usuario =>{
                    this.recurso = usuario;
        
                    // Obtengo los perfiles disponibles de la sucursal del usuario
                    this.perfiles = this.recursoService.getRecursoList(
                        resourcesREST.perfiles
                    )({
                        sucursal: this.recurso.perfil.sucursal.idSucursal
                    });
                });   
        });*/
        
    }

    /**
     * Se dispara cuando se cambia la sucursal en el dropdown
     * @param event 
    
    changeSucursal(event) {
        this.perfiles = this.recursoService.getRecursoList(
            resourcesREST.perfiles
        )({
            sucursal: this.recurso.perfil.sucursal.idSucursal
        });
    }
     */
    /**
     * Finaliza la creacion del user
    
    onClickEditarUsuario = async() => {
        
        //console.log(this.usuarioEnEdicion);

        try {
            // Edito el usuario seleccionado
            const resp = await this.recursoService.editarRecurso(
                this.recurso
            )({
                clave: crypto.MD5(this.recurso.clave),
                token: this.localStorageService.getObject(environment.localStorage.acceso).token
            });


            // Muestro mensaje de okey y redirecciono a la lista de usuarios
            this.utilsService.showModal(
                resp.control.codigo
            )(
                resp.control.descripcion
            )(
                () => this.router.navigate(['/pages/tablas/usuarios']) 
            )();
        }
        catch(ex) {
            this.utilsService.decodeErrorResponse(ex);
            
        }
    }
     */

}
