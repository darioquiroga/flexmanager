import { Component } from '@angular/core';

import { LocalStorageService } from '../../../../../../services/localStorageService';
import { Operador } from '../../../../../../models/operador';
import { Sitios } from 'app/models/sitios';
import { Perfil } from '../../../../../../models/perfil';
import { Sistemas } from '../../../../../../models/sistemas';
import { environment } from 'environments/environment';
import { UtilsService } from '../../../../../../services/utilsService';
import { Router } from '@angular/router';
import { isString } from 'util';
import { RecursoService } from '../../../../../../services/recursoService';

// Libreria para encriptar en MD5 la clave
import * as crypto from 'crypto-js';
import { resourcesREST } from 'constantes/resoursesREST';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'nuevo-operador',
    styleUrls: ['./nuevoOperador.scss'],
    templateUrl: './nuevoOperador.html',
})
export class NuevoOperador {
    // Perfiles disponible para tal sitios
    sitios: Observable<Sitios[]>;
    // Perfiles disponible para tal sitios
    perfiles: Observable<Perfil[]>;
    // Perfiles disponible para tal sitios
    sistemas: Observable<Sistemas[]>;
    
    // Usuario nuevo
    usuarioNuevo: Operador = new Operador();

    constructor(
        private utilsService: UtilsService,
        private router: Router,
        private recursoService: RecursoService,
        private localStorageService: LocalStorageService
    ) {
        // this.sucursales = recursoService.getRecursoList(resourcesREST.sucursales)();
        // this.perfiles = recursoService.getRecursoList(resourcesREST.perfiles)({
        //     sucursal: this.usuarioNuevo.perfil.sucursal.idSucursal
        // });
    }

    /**
     * Se dispara cuando se cambia la sucursal en el dropdown
     * @param event 
    
    
     changeSucursal(event) {        
        this.perfiles = this.recursoService.getRecursoList(
            resourcesREST.perfiles
        )({
            sucursal: this.usuarioNuevo.perfil.sucursal.idSucursal
        });
    }

    
    */
   
    /**
     * Finaliza la creacion del user
     */
   /* onClickCrearUsuario = async () => {
        
        try {
            // Creo el usuario nuevo
            const resp: any = await this.recursoService.setRecurso(
                this.usuarioNuevo
            )({
                clave: crypto.MD5(this.usuarioNuevo.clave),
                token: this.localStorageService.getObject(environment.localStorage.acceso).token
            });

            // Muestro mensaje de okey y redirecciono a la lista de usuarios
            this.utilsService.showModal(
                resp.control.codigo
            )(
                resp.control.descripcion
            )(
                () => this.router.navigate(['/pages/tablas/usuarios']) 
            )();
        }
        catch(ex) {
            this.utilsService.decodeErrorResponse(ex);
        }
    }

    
    compareWithSucursal(item1: Sucursal, item2: Sucursal) {
        return item1.idSucursal === item2.idSucursal;
    }

    compareWithPerfil(item1: Perfil, item2: Perfil) {
        return item1.idPerfil === item2.idPerfil;
    }
*/
}
