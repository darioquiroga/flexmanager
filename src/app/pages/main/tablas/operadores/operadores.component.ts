import { Component } from '@angular/core';
import { environment } from 'environments/environment';
import { Router } from '@angular/router';
import { Operador } from 'app/models/operador';
import { UtilsService } from '../../../../services/utilsService';
import { RecursoService } from 'app/services/recursoService';
import { resourcesREST } from 'constantes/resoursesREST';

@Component({
    selector: 'maps',
    styleUrls: ['./operadores.scss'],
    templateUrl: './operadores.html'
})
export class Operadores {

    // Data de la tabla
    dataOperadores;

    // Columnas de la tabla
    tableColumns;

    constructor(
        private router: Router,
        private utilsService: UtilsService,
        private recursoService: RecursoService
    ) {
        // Guardo las columnas de la tabla con sus respectivas anchuras
        this.tableColumns = [
            {
                nombre: 'nombre',
                key: 'nombre',
                ancho: '30%'
            },
            {
                nombre: 'email',
                key: 'email',
                ancho: '30%'
            },
            {
                nombre: 'telefono',
                key: 'telefono',
                ancho: '30%'
            }
        ]
        // Obtengo la lista de usuarios
        this.dataOperadores= this.recursoService.getRecursoList(resourcesREST.operador)();
    }

    /**
     * Redireciona a la pagina de editar
     */
    onClickEdit = (operador) => {
        // Redirecciono al dashboard
        //this.router.navigate(['/pages/tablas/usuarios/editar', operador.idKey]);
    }

    /**
     * Borra el usuario y muestra un mensajito avisando tal accion
     */
    onClickRemove = async(operador) => {
        
        // Pregunto si está seguro
        this.utilsService.showModal(
            'Borrar Operador'
        )(
            '¿Estás seguro de borrar el Operador?'
        )(
           async () => {
                // Borro usuario
                //const resp = await this.recursoService.borrarRecurso(Operador.idKey)(resourcesREST.operador);
                
                // Obtengo la lista de usuarios actualizada
                //this.dataUsuarios = this.recursoService.getRecursoList(resourcesREST.Operador)();
            }
        )({
            tipoModal: 'confirmation'
        });
    }

}
