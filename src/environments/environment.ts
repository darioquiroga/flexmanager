// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    FlexManagerRest: {
        // urlBase: 'http://www.kernelinformatica.com.ar/app/webservices/flexmanager/Auth.php',
        urlBase: 'http://www.kernelinformatica.com.ar/webservices/flexmanager/',
        //urlBase: 'http://10.0.0.30:8080/FacturacionRest/ws',
        timeoutDefault: 60000  //60 seg
    },
    localStorage: {
        acceso: 'accesoActivo',
        menu: 'menuActivo',
        perfil: 'perfilActivo',
        operador: 'operadorActivo'
    }
};
